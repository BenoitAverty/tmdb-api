package com.zenika.academy.tmdb;

import info.movito.themoviedbapi.TmdbApi;
import info.movito.themoviedbapi.model.MovieDb;
import info.movito.themoviedbapi.model.core.MovieResultsPage;
import info.movito.themoviedbapi.model.people.PersonCast;

import java.io.*;
import java.time.LocalDate;
import java.time.Year;
import java.util.List;
import java.util.Optional;
import java.util.Random;
import java.util.stream.Collectors;

public class TmdbClient {

    private final TmdbApi client;

    /**
     * Builds a client that uses the given api key with a file.
     * <p>
     * The api key must be in a text file with no other content.
     *
     * @param apiKeyFile the file where the api key lies.
     * @throws IOException if the file can't be read or can't be found.
     */
    public TmdbClient(File apiKeyFile) throws IOException {
        String apiKey = new String(new FileInputStream(apiKeyFile).readAllBytes());
        this.client = new TmdbApi(apiKey);
    }

    /**
     * Builds a client that uses the given api key with an InputStream.
     * <p>
     * The api key must be in a text file with no other content.
     *
     * @param inputStream the inputStream in which the api key lies.
     * @throws IOException if the file can't be read or can't be found.
     */
    public TmdbClient(InputStream inputStream) throws IOException {
        StringBuilder apiKey = new StringBuilder();
        BufferedReader reader = new BufferedReader(new InputStreamReader(inputStream));
        String line;
        while ((line = reader.readLine()) != null) {
            apiKey.append(line);
        }
        client = new TmdbApi(apiKey.toString());
    }

    /**
     * Get info about the movie given in the parameters
     */
    public Optional<MovieInfo> getMovie(String title) {
        final MovieResultsPage movieDbs = this.client.getSearch().searchMovie(title, null, "", false, 1);
        try {
            final MovieDb rawMovie = movieDbs.getResults().get(0);
            return tryGetAnswer(rawMovie);
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    /**
     * Get Random Popular movie info from Tmdb popular movie list
     */
    public Optional<MovieInfo> getRandomPopularMovie() {
        final MovieResultsPage movieDbs = this.client.getMovies().getPopularMovies("", 1);
        try {
            final MovieDb rawMovie = movieDbs.getResults().get(new Random().nextInt(movieDbs.getResults().size()));
            return tryGetAnswer(rawMovie);
        } catch (IndexOutOfBoundsException e) {
            return Optional.empty();
        }
    }

    private Optional<MovieInfo> tryGetAnswer(MovieDb rawMovie) {
            final List<PersonCast> rawCast = this.client.getMovies().getCredits(rawMovie.getId()).getCast();
            final Year releaseYear = Year.from(LocalDate.parse(rawMovie.getReleaseDate()));
            List<MovieInfo.Character> cast = rawCast.stream()
                    .map(personCast -> new MovieInfo.Character(personCast.getCharacter(), personCast.getName()))
                    .collect(Collectors.toList());
            return Optional.of(new MovieInfo(rawMovie.getTitle(), releaseYear, cast));
    }
}
